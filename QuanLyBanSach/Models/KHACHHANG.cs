//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuanLyBanSach.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class KHACHHANG
    {
        public KHACHHANG()
        {
            this.DONHANG = new HashSet<DONHANG>();
        }
    
        public int MAKH { get; set; }
        public string TENKH { get; set; }
        public string MATKHAU { get; set; }
        public string EMAIL { get; set; }
        public string DIENTHOAI { get; set; }
    
        public virtual ICollection<DONHANG> DONHANG { get; set; }
    }
}
