﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuanLyBanSach.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class CHITIETDONHANG_KDN
    {
        public int MADONHANG_KHONGDN { get; set; }
        public int MASP { get; set; }
        public Nullable<int> SOLUONGMUA { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:#,###} VNĐ")]
        public Nullable<decimal> THANHTIEN { get; set; }
    
        public virtual DONHANG_KDN DONHANG_KDN { get; set; }
        public virtual SANPHAM SANPHAM { get; set; }
    }
}
