﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Models
{
    public class GioHang
    {
        private QL_BSEntities  db = new QL_BSEntities();
        public int MaSP { get; set; }
        public string TenSP { get; set; }

        public string ANHSP { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:#,###}VNĐ")]
        public decimal? DonGia { get; set; }
        public int SoLuong { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:#,###}VNĐ")]
        public decimal? ThanhTien {
            get
            {
                return SoLuong * DonGia;
            }
        }
        public GioHang(int id,bool khuyenMai = false)
        {
            if (khuyenMai)
            {
                var thongTinKM = db.THONGTINKHUYENMAI.Single(t => t.MAKM == id);
                SANPHAM sanPham = thongTinKM.SANPHAM;
                MaSP = sanPham.MASP;
                TenSP = sanPham.TENSP;
                ANHSP = sanPham.ANHSP;
                DonGia = thongTinKM.THANHTIEN;
                SoLuong = 1;
            } else
            {
                this.MaSP = id;
                SANPHAM sanPham = db.SANPHAM.Single(n => n.MASP == MaSP);
                TenSP = sanPham.TENSP;
                ANHSP = sanPham.ANHSP;
                DonGia = sanPham.GIABAN;
                SoLuong = 1;
            }
            
        }
    }
}