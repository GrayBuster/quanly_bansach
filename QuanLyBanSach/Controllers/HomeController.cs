﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Controllers
{
    public class HomeController : Controller
    {
        private QL_BSEntities db = new QL_BSEntities();

        private List<SANPHAM> listProduct = new List<SANPHAM>();

        // GET: Product
        [HttpGet, ActionName("TrangChu")]
        public ActionResult Index()
        {
            var sANPHAM = db.SANPHAM.Include(s => s.LOAISACH).Include(s => s.NHACUNGCAP).Include(s => s.THONGTINKHUYENMAI);
            return View(sANPHAM.ToList());
        }

        public ActionResult BestSeller()
        {
            
            Random rnd = new Random();
            var listOfProduct = db.THONGTINKHUYENMAI.Include(s => s.SANPHAM).ToList();
            //var count = 4;
            //if (listProduct.Count == 0)
            //{
            //    for (int i = 0; i < count; i++)
            //    {
            //        var rndNumber = rnd.Next(1, db.SANPHAM.Count());
            //        var selectedPro = db.SANPHAM.ToList().ElementAt(rndNumber);
            //        if (!listProduct.Contains(selectedPro))
            //        {
            //            listProduct.Add(selectedPro);
            //        } else
            //        {
            //            count++;
            //        }

            //    }
            //}
            //ViewData["BestSeller"] = listProduct;
            return PartialView(listOfProduct);
        }

        [Route]
        [ActionName("GioiThieu")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Route]
        // GET: Home/Chi Tiết
        [ActionName("ChiTietSanPham")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SANPHAM sanPham = db.SANPHAM.Find(id);
            if (sanPham == null)
            {
                return HttpNotFound();
            }
            return View(sanPham);
        }

        // GET: Home/Chi Tiết
        public ActionResult ChiTietSanPhamKM(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            THONGTINKHUYENMAI khuyenMai = db.THONGTINKHUYENMAI.Find(id);
            if (khuyenMai == null)
            {
                return HttpNotFound();
            }
            return View(khuyenMai);
        }

        [Route]
        // GET: SảnPhẩm/Details/5
        [ActionName("SanPhamLienQuan")]
        public ActionResult SanPhamLienQuan(int? id)
        {
            SANPHAM sanPham = db.SANPHAM.Single(sp => sp.MASP == id);
            Random random = new Random();
            var next = random.Next();
            List<SANPHAM> listSanPham = new List<SANPHAM>();
            listSanPham = db.SANPHAM.Where(sp => sp.MALOAI == sanPham.MALOAI && sp.MASP != id).OrderBy(s => next).Take(3).ToList();
            //foreach (var item in db.SANPHAM)
            //{
            //    if (item.MALOAI == sanPham.MALOAI)
            //    {
            //        listSanPham.Add(item);
            //    }
            //}
            return PartialView(listSanPham);
        }


        // GET: Product/Create
        public ActionResult Create()
        {
            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI");
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC");
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MASP,TENSP,MALOAI,MANCC,MOTA,SOLUONGTON,TINHTRANG_HANG,GIABAN")] SANPHAM sANPHAM)
        {
            if (ModelState.IsValid)
            {
                db.SANPHAM.Add(sANPHAM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI", sANPHAM.MALOAI);
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC", sANPHAM.MANCC);
            return View(sANPHAM);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SANPHAM sANPHAM = db.SANPHAM.Find(id);
            if (sANPHAM == null)
            {
                return HttpNotFound();
            }
            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI", sANPHAM.MALOAI);
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC", sANPHAM.MANCC);
            return View(sANPHAM);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MASP,TENSP,MALOAI,MANCC,MOTA,SOLUONGTON,TINHTRANG_HANG,GIABAN")] SANPHAM sANPHAM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sANPHAM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI", sANPHAM.MALOAI);
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC", sANPHAM.MANCC);
            return View(sANPHAM);
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SANPHAM sANPHAM = db.SANPHAM.Find(id);
            if (sANPHAM == null)
            {
                return HttpNotFound();
            }
            return View(sANPHAM);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SANPHAM sANPHAM = db.SANPHAM.Find(id);
            db.SANPHAM.Remove(sANPHAM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
