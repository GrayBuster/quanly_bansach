﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Controllers
{
    public class CustomerController : Controller
    {
        private QL_BSEntities db = new QL_BSEntities();

        // GET: KHACHHANGs
        [HttpGet,ActionName("QuanLyKhachHang")]
        public ActionResult Index()
        {
            return View(db.KHACHHANG.ToList());
        }
        [Route]
        // GET: KHACHHANGs/Details/5
        [ActionName("ThongTinKhachHang")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KHACHHANG kHACHHANG = db.KHACHHANG.Find(id);
            if (kHACHHANG == null)
            {
                return HttpNotFound();
            }
            return View(kHACHHANG);
        }

        // GET: KHACHHANGs/Create
        [Route]
        [ActionName("Register")]
        public ActionResult Create()
        {
            return View("Register");
        }

        // POST: KHACHHANGs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route]
        [HttpPost]
        [ActionName("Register")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MAKH,TENKH,EMAIL,MATKHAU,DIENTHOAI")] KHACHHANG kHACHHANG,FormCollection form)
        {
            var gioiTinh = form["gioiTinh"];
            var confirmedPassword = form["confirmedPassword"];
            KHACHHANG kh1 = db.KHACHHANG.SingleOrDefault(n => n.EMAIL == kHACHHANG.EMAIL);
            if (kh1 == null)
            {
                if (!String.IsNullOrEmpty(confirmedPassword))
                {
                    if (confirmedPassword.Equals(kHACHHANG.MATKHAU))
                    {
                        if (ModelState.IsValid)
                        {
                            db.KHACHHANG.Add(kHACHHANG);
                            db.SaveChanges();
                            return View("DangKyThanhCong");
                        }
                    }
                    else
                    {
                        ViewData["ConfirmedPassword"] = "Confirmed Password không trùng với Password";
                    }
                }else
                {
                    ViewBag.PasswordRong = "Confirmed Password không được bỏ trống";
                }
                
                
            }else
            {
                ViewBag.Email = "Email đã tồn tại .Mời bạn nhập email khác";
            }
            

            return View(kHACHHANG);
        }
        
        [HttpGet]
        [Route]
        [ActionName("Login")]
        public ActionResult DangNhap()
        {
            return View("Login");
        }
        
        [HttpPost]
        [Route]
        [ActionName("Login")]
        public ActionResult DangNhap(FormCollection collection)
        {
            var email = collection["email"];
            var password = collection["password"];
            var rememberMe = collection["rememberMe"];
            if (String.IsNullOrEmpty(email))
            {
                ViewData["Loi1"] = "Email không được bỏ trống";
                
            } else if (String.IsNullOrEmpty(password)) {
                ViewData["Loi2"] = "Password không được bỏ trống";
            } else {
                KHACHHANG kh = db.KHACHHANG.SingleOrDefault(n => n.EMAIL == email && n.MATKHAU == password);
                if (kh != null)
                {
                    if (rememberMe != null)
                    {
                        if (rememberMe.Equals("true"))
                        {
                            Session["email"] = kh.EMAIL;
                            Session["password"] = kh.MATKHAU;
                        }
                    } else
                    {
                        Session["email"] = null;
                        Session["password"] = null;
                    }
                    Session["TaiKhoan"] = kh;
                    return RedirectToAction("TrangChu", "Home");
                } else
                {
                    ViewBag.ThongBao = "Email hoặc password không chính xác";
                }
            }
           
            return View();
        }
        [Route]
        [ActionName("Logout")]
        public ActionResult DangXuat()
        {
            Session["TaiKhoan"] = null;
            return RedirectToAction("TrangChu","Home");
        }

        // GET: KHACHHANGs/Edit/5
        [ActionName("ChinhSua")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KHACHHANG kHACHHANG = db.KHACHHANG.Find(id);
            if (kHACHHANG == null)
            {
                return HttpNotFound();
            }
            return View(kHACHHANG);
        }

        // POST: KHACHHANGs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ActionName("ChinhSua")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MAKH,TENKH,HOKH,EMAIL,MATKHAU,DIENTHOAIKH")] KHACHHANG kHACHHANG,FormCollection form)
        {
            var gioiTinh = form["gioiTinh"];
            if (ModelState.IsValid)
            {
                db.Entry(kHACHHANG).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kHACHHANG);
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
