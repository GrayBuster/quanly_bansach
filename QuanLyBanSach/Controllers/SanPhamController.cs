﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Controllers
{
    public class SanPhamController : Controller
    {
        private QL_BSEntities db = new QL_BSEntities();

        // GET: SanPham
        [ActionName("QuanLySanPham")]
        public ActionResult Index()
        {
            var sANPHAM = db.SANPHAM.Include(s => s.LOAISACH).Include(s => s.NHACUNGCAP);
            return View(sANPHAM.ToList());
        }

        // GET: SanPham/Create
        [ActionName("ThemSanPham")]
        public ActionResult Create()
        {
            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI");
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC");
            return View();
        }

        // POST: SanPham/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ActionName("ThemSanPham")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MASP,TENSP,MALOAI,MANCC,MOTA,SOLUONGTON,TINHTRANG_HANG,GIABAN,ANHSP")] SANPHAM sanPham, HttpPostedFileBase file)
        {
            sanPham.TINHTRANG_HANG = true;
            if (sanPham.SOLUONGTON <= 0) {
                ViewBag.ValidateSoLuong = "Số lượng không được nhỏ hơn 0";
                return View(sanPham);
            }
            this.uploadFile(sanPham,file);
            if (ModelState.IsValid)
            {
                db.SANPHAM.Add(sanPham);
                db.SaveChanges();
                return RedirectToAction("QuanLySanPham");
            }

            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI", sanPham.MALOAI);
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC", sanPham.MANCC);
            return View(sanPham);
        }

        void uploadFile(SANPHAM sanPham, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                sanPham.ANHSP = fileName;
                var path = Path.Combine(Server.MapPath("~/images/"), fileName);
                file.SaveAs(path);
            } else
            {
                sanPham.ANHSP = "";
            }
        }

        // GET: SanPham/Edit/5
        [ActionName("SuaSanPham")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SANPHAM sANPHAM = db.SANPHAM.Find(id);
            if (sANPHAM == null)
            {
                return HttpNotFound();
            }
            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI", sANPHAM.MALOAI);
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC", sANPHAM.MANCC);
            return View(sANPHAM);
        }

        // POST: SanPham/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ActionName("SuaSanPham")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MASP,TENSP,MALOAI,MANCC,MOTA,SOLUONGTON,TINHTRANG_HANG,GIABAN,ANHSP")] SANPHAM sanPham, HttpPostedFileBase file)
        {
            sanPham.TINHTRANG_HANG = true;
            if (sanPham.SOLUONGTON <= 0)
            {
                ViewBag.ValidateSoLuong = "Số lượng không được nhỏ hơn 0";
                return View(sanPham);
            }
            this.uploadFile(sanPham, file);
            if (ModelState.IsValid)
            {
                db.Entry(sanPham).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("QuanLySanPham");
            }
            ViewBag.MALOAI = new SelectList(db.LOAISACH, "MALOAI", "TENLOAI", sanPham.MALOAI);
            ViewBag.MANCC = new SelectList(db.NHACUNGCAP, "MANCC", "TENNCC", sanPham.MANCC);
            return View(sanPham);
        }

        // GET: SanPham/Delete/5
        [ActionName("XoaSanPham")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SANPHAM sANPHAM = db.SANPHAM.Find(id);
            if (sANPHAM == null)
            {
                return HttpNotFound();
            }
            return View(sANPHAM);
        }

        // POST: SanPham/Delete/5
        [HttpPost, ActionName("XoaSanPham")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SANPHAM sANPHAM = db.SANPHAM.Find(id);
            db.SANPHAM.Remove(sANPHAM);
            db.SaveChanges();
            return RedirectToAction("QuanLySanPham");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
