﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuanLyBanSach.Models;
using System.Diagnostics;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace QuanLyBanSach.Controllers
{
    public class GioHangController : Controller
    {
        private QL_BSEntities db = new QL_BSEntities();
        public List<GioHang> LayGioHang()
        {
            List<GioHang> listGioHang = Session["GioHang"] as List<GioHang>;
            if (listGioHang == null)
            {
                listGioHang = new List<GioHang>();
                Session["GioHang"] = listGioHang;
            }
            return listGioHang;
        }
        [ActionName("ThêmGiỏHàng")]
        public ActionResult ThemGioHang(int id,bool? khuyenMai, string strURL)
        {
            if (khuyenMai != null)
            {
                List<GioHang> listGioHang = LayGioHang();
                GioHang sanPham = listGioHang.Find(n => n.MaSP == id);
                if (sanPham == null)
                {
                    sanPham = new GioHang(id,true);
                    listGioHang.Add(sanPham);
                    return Redirect(strURL);
                }
                else
                {
                    sanPham.SoLuong++;
                    return Redirect(strURL);
                }

            } else
            {
                List<GioHang> listGioHang = LayGioHang();
                GioHang sanPham = listGioHang.Find(n => n.MaSP == id);
                if (sanPham == null)
                {
                    sanPham = new GioHang(id);
                    listGioHang.Add(sanPham);
                    return Redirect(strURL);
                }
                else
                {
                    sanPham.SoLuong++;
                    return Redirect(strURL);
                }
            }
            
        }
        [ActionName("XóaSảnPhẩm")]
        public ActionResult XoaSanPham(int? maSP)
        {
            List<GioHang> listGioHang = LayGioHang();
            if (maSP == null)
            {
                ViewBag.MaSPRong = "Không tìm thấy sản phẩm này";
                return RedirectToAction("GiỏHàng");
            } else
            {
                GioHang sanPham = listGioHang.Find(n => n.MaSP == maSP);
                if (sanPham != null)
                {
                    listGioHang.RemoveAll(n => n.MaSP == maSP);
                    return RedirectToAction("GiỏHàng");
                }
                if (listGioHang.Count == 0)
                {
                    return RedirectToAction("GiỏHàng");
                }
                return RedirectToAction("GiỏHàng");
            }
        }
        public ActionResult CapNhatSoLuong(int maSP,FormCollection collection)
        {
            List<GioHang> listGioHang = LayGioHang();
            GioHang sanPham = listGioHang.SingleOrDefault(n => n.MaSP == maSP);
            if (sanPham != null)
            {
                sanPham.SoLuong = int.Parse(collection["txtSoLuong"].ToString());
            }
            return RedirectToAction("GiỏHàng");
        }
        private int TongSoLuong()
        {
            int tongSoLuong = 0;
            List<GioHang> listGioHang = Session["GioHang"] as List<GioHang>;
            if (listGioHang != null)
            {
                tongSoLuong = listGioHang.Sum(n => n.SoLuong);
            }
            return tongSoLuong;
        }
        private decimal? TongTien()
        {
            decimal? tongTien = 0;
            List<GioHang> listGioHang = Session["GioHang"] as List<GioHang>;
            if (listGioHang != null)
            {
                tongTien = listGioHang.Sum(n => n.ThanhTien);
            }
            return tongTien;
        }
        [ActionName("GiỏHàng")]
        public ActionResult GioHang()
        {
            List<GioHang> listGioHang = LayGioHang();
            if (listGioHang.Count == 0)
            {
                ViewBag.GioRong = "Bạn hiện chưa có sản phẩm nào trong giỏ";
            }
            ViewBag.TongSoLuong = TongSoLuong();
            ViewBag.TongTien = TongTien();
            return View(listGioHang);
        }
        public ActionResult GioHangPartial()
        {
            ViewBag.TongSoLuong = TongSoLuong();
            ViewBag.TongTien = TongTien();
            return PartialView();
        }

        [ActionName("ĐặtHàngKhôngĐăngNhập")]
        [HttpGet]
        public ActionResult DatHangKhongDangNhap()
        {
            return View("ĐặtHàngKhôngĐăngNhập");
        }

        [ActionName("ĐặtHàngKhôngĐăngNhập")]
        [HttpPost]
        public ActionResult DatHangKhongDangNhap([Bind(Include = "DIACHIGIAOHANG,NGAYDATHANG,NGAYGIAOHANG,TINHTRANG,DATHANHTOAN,TENKH,EMAILKH,DIENTHOAIKH,TONGTIEN")]DONHANG_KDN donHangKhongDN)
        {
            List<GioHang> listGioHang = LayGioHang();
            donHangKhongDN.TONGTIEN = TongTien();
            donHangKhongDN.NGAYDATHANG = DateTime.Now;
            donHangKhongDN.NGAYGIAOHANG = DateTime.Now.AddDays(2);
            donHangKhongDN.TINHTRANG = false;
            donHangKhongDN.DATHANHTOAN = false;
            if (ModelState.IsValid)
            {
                db.DONHANG_KDN.Add(donHangKhongDN);
                db.SaveChanges();
            }
            foreach (var item in listGioHang)
            {
                CHITIETDONHANG_KDN chiTietDonHangKDN = new CHITIETDONHANG_KDN();
                chiTietDonHangKDN.MADONHANG_KHONGDN = donHangKhongDN.MADONHANG_KHONGDN;
                chiTietDonHangKDN.MASP = item.MaSP;
                SANPHAM sanPham = db.SANPHAM.SingleOrDefault(n => n.MASP == item.MaSP);
                sanPham.SOLUONGTON -= item.SoLuong;
                chiTietDonHangKDN.SOLUONGMUA = item.SoLuong;
                chiTietDonHangKDN.THANHTIEN = item.ThanhTien;
                db.CHITIETDONHANG_KDN.Add(chiTietDonHangKDN);
                donHangKhongDN.CHITIETDONHANG_KDN.Add(chiTietDonHangKDN);
            }
            db.SaveChanges();
            Session["GioHang"] = null;
            return View("XácNhậnĐơnHàngKDN",donHangKhongDN);
        }


        [HttpGet]
        [ActionName("ĐặtHàngĐãĐăngNhập")]
        public ActionResult DatHangDaDangNhap()
        {
            return View("ĐặtHàngĐãĐăngNhập");
        }

        [ActionName("ĐặtHàngĐãĐăngNhập")]
        [HttpPost]
        public ActionResult DatHangDaDangNhap([Bind(Include = "DIACHIGIAOHANG,NGAYDATHANG,NGAYGIAOHANG,TINHTRANG,DATHANHTOAN,MAKH,TONGTIEN")]DONHANG donHang)
        {
            List<GioHang> listGioHang = LayGioHang();
            donHang.TONGTIEN = TongTien();
            donHang.NGAYDATHANG = DateTime.Now;
            donHang.NGAYGIAOHANG = DateTime.Now.AddDays(2);
            donHang.TINHTRANG = false;
            donHang.DATHANHTOAN = false;
            if (Session["TaiKhoan"] != null)
            {
                var khachHang = Session["TaiKhoan"] as KHACHHANG;
                donHang.MAKH = khachHang.MAKH;
                if (ModelState.IsValid)
                {
                    try
                    {
                        db.DONHANG.Add(donHang);
                        db.SaveChanges();
                    }
                    catch(DbEntityValidationException ex)
                    {
                        foreach (var eve in ex.EntityValidationErrors)
                        {
                            Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    catch (DbUpdateException e)
                    {
                        foreach (var eve in e.Entries)
                        {
                            Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entity.GetType().Name, eve.State);
                            
                        }
                        throw;
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                        throw;
                    }


                }
            }
            
            foreach (var item in listGioHang)
            {
                CHITIETDONHANG chiTietDonHang = new CHITIETDONHANG();
                chiTietDonHang.MADONHANG = donHang.MADONHANG;
                chiTietDonHang.MASP = item.MaSP;
                SANPHAM sanPham = db.SANPHAM.SingleOrDefault(n => n.MASP == item.MaSP);
                sanPham.SOLUONGTON -= item.SoLuong;
                chiTietDonHang.SOLUONGMUA = item.SoLuong;
                chiTietDonHang.THANHTIEN = item.ThanhTien;
                donHang.CHITIETDONHANG.Add(chiTietDonHang);
                db.CHITIETDONHANG.Add(chiTietDonHang);
            }
            
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            Session["GioHang"] = null;
            return View("XácNhậnĐơnHàng", donHang);
        }


        [ActionName("XácNhậnĐơnHàng")]
        public ActionResult XacNhanDonHangKDN()
        {
            return View("XácNhậnĐơnHàng");
        }
    }
}