create database QL_BanSach
go
use QL_BanSach
go

create table LOAISACH
(
	MALOAI int identity(1,1),
	TENLOAI varchar(10) not null
	constraint PK_LOAISP_MALOAI primary key (MALOAI)
)

create table NHACUNGCAP
(
	MANCC int identity(1,1),
	TENNCC nvarchar(50),
	constraint PK_NHACUNGCAP_MANCC primary key (MANCC)
)

create table SANPHAM
(
	MASP int identity(1,1),
	TENSP nvarchar(50) not null,
	MALOAI int not null,
	MANCC int not null,
	MOTA nvarchar(100),
	SOLUONGTON int not null default 0,
	TINHTRANG_HANG bit not null default 0,
	GIABAN decimal(12) not null default 0
	constraint PK_SANPHAM_MASP primary key (MASP),
	constraint FK_SANPHAM_MANCC foreign key (MANCC) references NHACUNGCAP (MANCC),
	constraint FK_SANPHAM_MALOAI foreign key (MALOAI) references LOAISACH (MALOAI)
)

create table THONGTINKHUYENMAI
(
	MAKM int identity(1,1),
	MASP int not null,
	NGAYBATDAU datetime,
	NGAYKETTHUC datetime,
	THANHTIEN decimal,
	constraint PK_TTKM_MAKM_MASP primary key (MAKM, MASP),
	constraint FK_TTKM_MASP foreign key (MASP) references SANPHAM (MASP)
)

create table KHACHHANG
(
	MAKH int identity(1,1),
	TENKH nvarchar(50),
	MATKHAU varchar(50) not null,
	EMAIL varchar(30) unique,
	DIENTHOAI varchar(11) not null
	constraint PK_KHACHHANG_MAKH primary key (MAKH)
)

create table DONHANG
(
	MADONHANG int identity(1,1),
	DIACHIGIAOHANG varchar(100),
	NGAYDATHANG datetime,
	NGAYGIAOHANG datetime,
	TINHTRANG bit,
	DATHANHTOAN bit,
	MAKH int,
	TONGTIEN decimal,
	constraint PK_DONHANG_MADONHANG primary key (MADONHANG),
	constraint FK_DONHANG_MAKH foreign key (MAKH) references KHACHHANG(MAKH)
)

create table CHITIETDONHANG
(
	MADONHANG int,
	MASP int,
	SOLUONGMUA int check (SOLUONGMUA > 0),
	THANHTIEN decimal
	constraint PK_CTDH_MADONHANG_MASP primary key (MADONHANG, MASP),
	constraint FK_CTHD_MADONHANG foreign key (MADONHANG) references DONHANG (MADONHANG),
	constraint FK_CTHD_MASP foreign key (MASP) references SANPHAM (MASP)
)

create table DONHANG_KDN
(
	MADONHANG_KHONGDN int identity(1,1),
	DIACHIGIAOHANG varchar(100),
	NGAYDATHANG datetime,
	NGAYGIAOHANG datetime,
	TINHTRANG bit,
	DATHANHTOAN bit,
	TENKH nvarchar(50),
	EMAILKH varchar(30),
	DIENTHOAIKH varchar(11) not null,
	TONGTIEN decimal default 0,
	constraint PK_DONHANG_MADONHANG_KHONGDANGNHAP primary key (MADONHANG_KHONGDN),
)

create table CHITIETDONHANG_KDN
(
	MADONHANG_KHONGDN int,
	MASP int,
	SOLUONGMUA int check (SOLUONGMUA > 0),
	THANHTIEN decimal
	constraint PK_CTDH_MADONHANG_KDN_MASP primary key (MADONHANG_KHONGDN, MASP),
	constraint FK_CTHD_MADONHANG_KDN foreign key (MADONHANG_KHONGDN) references DONHANG_KDN (MADONHANG_KHONGDN),
	constraint FK_CTHD_MASP_KDN foreign key (MASP) references SANPHAM (MASP)
)




--create table HOADON
--(
--	MAHOADON int identity(1,1),
--	NGAYLAPHD datetime not null,
--	TONGTIENHOADON decimal default 0,
--	TENKH nvarchar(50),
--	EMAILKH varchar(30),
--	DIENTHOAIKH varchar(11) not null
--	constraint PK_HOADON_MAHOADON primary key (MAHOADON),
--)

--create table CHITIETHOADON
--(
--	MAHOADON int,
--	MASP int,
--	SOLUONGMUA int,
--	THANHTIEN decimal
--	constraint PK_CHITIETHOADON_MAHOADON primary key (MAHOADON, MASP),
--	constraint FK_CHITIETHOADON_MAHOADON foreign key (MAHOADON) references HOADON (MAHOADON),
--	constraint FK_CHITIETHOADON_MASP foreign key (MASP) references SANPHAM (MASP)
--)

go
--MARK: Triger for CHITIETDONHANG
create trigger TG_MUASP_ONLINE
on CHITIETDONHANG
for insert
as
begin
	declare @ma_hd int = (select MADONHANG from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @masp int = (select MASP from inserted)
	declare @giaban decimal = (select GIABAN from SANPHAM where MASP = @masp)
	update CHITIETDONHANG set THANHTIEN = @soluong * @giaban where (MADONHANG = @ma_hd and MASP = @masp)
	declare @thanhtien decimal = (select THANHTIEN from CHITIETDONHANG where MADONHANG = @ma_hd and MASP = @masp)
	update DONHANG set TONGTIEN = TONGTIEN + @thanhtien where MADONHANG = @ma_hd	
end
go

create trigger TG_MUASP_ONLINE_DELETE
on CHITIETDONHANG
for delete
as
begin
	declare @ma_hd int = (select MADONHANG from deleted)
	declare @thanhtien decimal = (select THANHTIEN from deleted)
	update DONHANG set TONGTIEN = TONGTIEN - @thanhtien where MADONHANG = @ma_hd	
end
go

create trigger TG_MUASP_ONLINE_UPDATE
on CHITIETDONHANG
for update
as
begin
	declare @ma_hd int = (select MADONHANG from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	declare @masp int = (select MASP from inserted)
	declare @giaban decimal = (select GIABAN from SANPHAM where MASP = @masp)
	update CHITIETDONHANG set THANHTIEN = @soluong * @giaban where (MADONHANG = @ma_hd and MASP = @masp)
	declare @thanhtien decimal = (select THANHTIEN from CHITIETDONHANG where MADONHANG = @ma_hd and MASP = @masp)
	declare @thanhtien_cu decimal = @soluong_cu * @giaban
	update DONHANG set TONGTIEN = TONGTIEN- @thanhtien_cu +  @thanhtien where MADONHANG = @ma_hd	
end
go

create trigger TG_MUASP_ONLINE_SANPHAM
on CHITIETDONHANG
for update
as
begin
	declare @ma_sp int = (select MASP from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	update SANPHAM set SOLUONGTON = SOLUONGTON - @soluong + @soluong_cu where (MASP = @ma_sp)
end
go

create trigger TG_MUASP_ONLINE_SANPHAM_DELETE
on CHITIETDONHANG
for delete
as
begin
	declare @ma_sp int = (select MASP from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	update SANPHAM set SOLUONGTON = SOLUONGTON + @soluong_cu where (MASP = @ma_sp)
end
go


create trigger TG_MUASP_ONLINE_SANPHAM_UP
on CHITIETDONHANG
for update
as
begin
	declare @ma_sp int = (select MASP from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	update SANPHAM set SOLUONGTON = SOLUONGTON - @soluong where (MASP = @ma_sp)
end
go

--MARK: Triger for CHITIETDONHANG_KDN

create trigger TG_MUASP_ONLINE_KDN
on CHITIETDONHANG_KDN
for insert
as
begin
	declare @ma_hd int = (select MADONHANG_KHONGDN from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @masp int = (select MASP from inserted)
	declare @giaban decimal = (select GIABAN from SANPHAM where MASP = @masp)
	update CHITIETDONHANG_KDN set THANHTIEN = @soluong * @giaban where (MADONHANG_KHONGDN = @ma_hd and MASP = @masp)
	declare @thanhtien decimal = (select THANHTIEN from CHITIETDONHANG_KDN where MADONHANG_KHONGDN = @ma_hd and MASP = @masp)
	update DONHANG_KDN set TONGTIEN = TONGTIEN + @thanhtien where MADONHANG_KHONGDN = @ma_hd	
end
go

create trigger TG_MUASP_ONLINE_KDN_DELETE
on CHITIETDONHANG_KDN
for delete
as
begin
	declare @ma_hd int = (select MADONHANG_KHONGDN from deleted)
	declare @thanhtien decimal = (select THANHTIEN from deleted)
	update DONHANG_KDN set TONGTIEN = TONGTIEN - @thanhtien where MADONHANG_KHONGDN = @ma_hd	
end
go

create trigger TG_MUASP_ONLINE_UPDATE_KDN
on CHITIETDONHANG_KDN
for update
as
begin
	declare @ma_hd int = (select MADONHANG_KHONGDN from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	declare @masp int = (select MASP from inserted)
	declare @giaban decimal = (select GIABAN from SANPHAM where MASP = @masp)
	update CHITIETDONHANG_KDN set THANHTIEN = @soluong * @giaban where (MADONHANG_KHONGDN = @ma_hd and MASP = @masp)
	declare @thanhtien decimal = (select THANHTIEN from CHITIETDONHANG_KDN where MADONHANG_KHONGDN = @ma_hd and MASP = @masp)
	declare @thanhtien_cu decimal = @soluong_cu * @giaban
	update DONHANG_KDN set TONGTIEN = TONGTIEN- @thanhtien_cu +  @thanhtien where MADONHANG_KHONGDN = @ma_hd	
end
go

create trigger TG_MUASP_ONLINE_KDN_SANPHAM
on CHITIETDONHANG_KDN
for update
as
begin
	declare @ma_sp int = (select MASP from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	update SANPHAM set SOLUONGTON = SOLUONGTON - @soluong + @soluong_cu where (MASP = @ma_sp)
end
go

create trigger TG_MUASP_ONLINE_KDN_SANPHAM_DELETE
on CHITIETDONHANG_KDN
for delete
as
begin
	declare @ma_sp int = (select MASP from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	update SANPHAM set SOLUONGTON = SOLUONGTON + @soluong_cu where (MASP = @ma_sp)
end
go


create trigger TG_MUASP_ONLINE_KDN_SANPHAM_UP
on CHITIETDONHANG_KDN
for update
as
begin
	declare @ma_sp int = (select MASP from inserted)
	declare @soluong int = (select SOLUONGMUA from inserted)
	declare @soluong_cu int = (select SOLUONGMUA from deleted)
	update SANPHAM set SOLUONGTON = SOLUONGTON - @soluong where (MASP = @ma_sp)
end
go

--Insert SANPHAM--
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Dragon Ball Super', 1, 1 , 5, N'Kh�ng c�',13000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Bleach', 2, 1 , 5, N'Kh�ng c�',15000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Attack on titan', 2, 1 , 5, N'Kh�ng c�',20000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Gintama 2', 2, 1 , 5, N'Kh�ng c�',15000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Neko Doku', 2, 1 , 5, N'Kh�ng c�',14000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Nichijou 5', 2, 1 , 5, N'Kh�ng c�',18000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Purizuma', 2, 1 , 5, N'Kh�ng c�',21000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Raibui 2', 2, 1 , 5, N'Kh�ng c�',16000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'SuzukiKun Suki', 2, 1 , 5, N'Kh�ng c�',18000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'ToKegurui', 2, 1 , 5, N'Kh�ng c�',19000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Toshokan', 2, 1 , 5, N'Kh�ng c�',19000,1)
insert into SANPHAM (TENSP, MANCC, MALOAI, SOLUONGTON, MOTA,GIABAN,TINHTRANG_HANG) values (N'Hunter X Hunter', 2, 1 , 5, N'Kh�ng c�',17000,1)
